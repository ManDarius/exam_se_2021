package Subjects;

import java.util.ArrayList;
import java.util.List;

public class SubjectOne {
    public static void main(String[] args) {
        User user = new User();

        CarMaker carMaker = new CarMaker();

        Car bmw = new Car("black", 50, 104.3f);
        Car porsche = new Car("red", 45, 106.9f);
        Car ferrari = new Car("blue", 76, 101.5f);

        bmw.carInfo();
        porsche.carInfo();
        ferrari.carInfo();

        carMaker.addCar(bmw);
        carMaker.addCar(porsche);
        carMaker.addCar(ferrari);

        user.goToWork(bmw);
    }
}

// Simple class
class Vehicle {

}

// The car class inherits the Vehicle class using the "extends" keyword
class Car extends Vehicle {
    private String color;

    private Wheel[] wheel;
    private Windshield windshield;

    public Car(String color, int wheelDiameter, float windshieldSurfaceArea) {
        this.color = color;

        wheel = new Wheel[4];
        wheel[0] = new Wheel();
        wheel[0].setWheelDiameter(wheelDiameter);
        wheel[1] = new Wheel();
        wheel[1].setWheelDiameter(wheelDiameter);
        wheel[2] = new Wheel();
        wheel[2].setWheelDiameter(wheelDiameter);
        wheel[3] = new Wheel();
        wheel[3].setWheelDiameter(wheelDiameter);

        windshield = new Windshield();
        windshield.setSurfaceArea(windshieldSurfaceArea);
    }

    public void start() {
        System.out.println("Car started.");
    }

    public void stop() {
        System.out.println("Car stopped.");
    }

    public void go() {
        System.out.println("Car moved a bit.");
    }

    public void carInfo() {
        System.out.println("The car is " + color + " and has a wheel diameter of " + wheel[0].getWheelDiameter() + " and a windshield surface area of " + windshield.getSurfaceArea());
    }
}

// The Car class has a composition with wheel and windshield and because of this a car cannot exist with out wheels and a windshield
class Wheel {
    private int wheelDiameter;

    public int getWheelDiameter() {
        return wheelDiameter;
    }

    public void setWheelDiameter(int wheelDiameter) {
        this.wheelDiameter = wheelDiameter;
    }
}

class Windshield {
    private float surfaceArea;

    public float getSurfaceArea() {
        return surfaceArea;
    }

    public void setSurfaceArea(float surfaceArea) {
        this.surfaceArea = surfaceArea;
    }
}

// The CarMaker class has an association with the class Car which means that the car maker uses the Car class
class CarMaker {
    private List<Car> carList;

    public CarMaker() {
        carList = new ArrayList<>();
    }

    public void addCar(Car car) {
        carList.add(car);
    }
}

// The user uses the Car class from time to time
class User {
    public void goToWork(Car personalCar) {
        personalCar.start();
        personalCar.go();
        personalCar.stop();
    }
}