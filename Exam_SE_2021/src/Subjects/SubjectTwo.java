package Subjects;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SubjectTwo {
    public static void main(String[] args) {
        new Window();
    }
}

// The windows that appears on the screen
class Window extends JFrame {

    // The button that will be pressed
    private JButton button;

    // The two text fields from which the text moves
    private JTextField initialField;
    private JTextField secondField;

    // Control variable used to determine how the text should move
    private int flag;

    public Window() {
        flag = 0;

        // Initialization of the window
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(200, 150);

        // Initialization of the components
        initialField = new JTextField("");
        initialField.setBounds(20, 20, 60, 25);
        add(initialField);

        secondField = new JTextField("");
        secondField.setBounds(90, 20, 60, 25);
        add(secondField);

        button = new JButton("Click me");
        button.setBounds(20, 55, 100, 25);
        button.addActionListener(new MoveTextListener());
        add(button);

        // Setting the window visible
        setVisible(true);
    }

    // Action listener class the will trigger each time the button is clicked
    class MoveTextListener implements ActionListener {

        // Moves the text from one filed to the other
        @Override
        public void actionPerformed(ActionEvent e) {
            switch (flag) {
                case 0: {
                    initialField.setText("man");
                    secondField.setText("");
                    flag = 1;
                }
                break;
                case 1: {
                    initialField.setText("");
                    secondField.setText("man");
                    flag = 0;
                }
                break;
            }
        }
    }
}